import dsa.utility.Util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;

public class Mains {
    private static class Group implements Comparable<Group> {
        int count;
        int start;
        int end;

        Group(int count, int start, int end) {
            this.count = 0;
            this.start = start;
            this.end = end;
        }

        @Override
        public int compareTo(Group o) {
            if (this.end != o.end) {
                return this.end - o.end;
            } else {
                return o.start - this.start;
            }
        }

        @Override
        public String toString() {
            return "Group{" +
                    "count=" + count +
                    ", start=" + start +
                    ", end=" + end +
                    '}';
        }
    }

    public static void main(String[] args) throws IOException {
//        result.end = 9;
//        System.out.println(groups);
//

//        fibonacciTriangle(5);

//        System.out.println(Math.exp(-1));

        /*int[][] grid ={
            {0,0,0},
            {0,1,1},
            {0,0,0}
        };

        Util.display(grid);

        System.out.println(uniquePathsWithObstacles(grid));

        Util.display(grid);*/
//        loadFile();
//        System.out.println(2%3);

//        String s = "90th";
//        int c = s.charAt(0);
//        System.out.println(c);
//        int[] arr = new int[] {1, 2, 2, 1, 0, 0, 0};
//        sort(arr);
//        Util.display(arr, false);
//
//        generateSQL();

//        int a = 2;
//        int b = 6;
//        double c = a/(double)b;
//        System.out.println(c);
//
//        HashSet<Employee> set = new HashSet<>();
//        set.add(new Employee(1, "Fame"));
//        set.add(new Employee(1, "Fame"));
//        System.out.println(set);
//
//        System.out.println(Integer.parseInt("02"));
        String s = "abc";
        for (int i = 1; i <= s.length(); i++) {
            System.out.println(s.substring(0, i));
            System.out.println("---------------------------");
        }
    }

    private static class Employee {
        int id;
        String name;

        public Employee(int id, String name) {
            this.id = id;
            this.name = name;
        }

//        @Override
//        public boolean equals(Object o) {
//            if (this == o) return true;
//            if (!(o instanceof Employee)) return false;
//            Employee employee = (Employee) o;
//            return id == employee.id && name.equals(employee.name);
//        }

        @Override
        public int hashCode() {
            return Objects.hash(id, name);
        }


        @Override
        public String toString() {
            return "Employee{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }


    private static void fibonacciTriangle(int n) {
        int a = 0;
        int b = 1;
        int sum = a + b;
        for (int i = 1; i <= n; ++i) {

            for (int j = 1; j <= n; ++j) {

                if (i == j) {

                    System.out.print(a);

                    a = sum;
                    sum = sum + a;
                }

            }
            System.out.println();

        }
    }

    public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        for (int i = obstacleGrid.length - 1; i >= 0; i--) {
            for (int j = obstacleGrid[i].length - 1; j >= 0; j--) {
                if ((i == obstacleGrid.length - 1 && j == obstacleGrid[i].length - 1) || i == obstacleGrid.length - 1 || j == obstacleGrid[i].length - 1) {
                    if (obstacleGrid[i][j] == 1) {
                        obstacleGrid[i][j] = -1;
                    } else {
                        obstacleGrid[i][j] = 1;
                    }
                    System.out.println("=================");
                    Util.display(obstacleGrid);
                    continue;
                }


                if (obstacleGrid[i][j] == 1) {
                    obstacleGrid[i][j] = -1;
                    continue;
                }

                if (obstacleGrid[i][j] != -1) {
                    obstacleGrid[i][j] = obstacleGrid[i + 1][j] == -1 ? 0 : obstacleGrid[i + 1][j];
                    obstacleGrid[i][j] += obstacleGrid[i][j + 1] == -1 ? 0 : obstacleGrid[i][j + 1];
                    System.out.println("------------------");
                    Util.display(obstacleGrid);
                }
            }
        }

        return obstacleGrid[0][0];
    }

    private static void loadFile() throws IOException {
        String defaultPNGPath = "src/default.png";
        File imgFile = new File(defaultPNGPath);
        byte[] res = Files.readAllBytes(imgFile.toPath());
        for (byte b : res) System.out.print(b);
//        System.out.println(new String(res));
    }

    private static void sort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                int temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
                i = -1;
            }
        }
    }

    public static void generateSQL() {
        String query = "INSERT INTO `configuration`.`user_roles` (`userID`, `roleID`) VALUES ('-1', '%d');";
        for (int i = 21; i <= 150; i++) {
            System.out.println(String.format(query, i));
        }

    }

}
